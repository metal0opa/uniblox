"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const mongoose_1 = __importDefault(require("mongoose"));
const signup_1 = __importDefault(require("./src/routes/signup"));
const signin_1 = __importDefault(require("./src/routes/signin"));
const checkout_1 = __importDefault(require("./src/routes/checkout"));
const addToCart_1 = __importDefault(require("./src/routes/addToCart"));
const createItem_1 = __importDefault(require("./src/routes/createItem"));
const adminSignup_1 = __importDefault(require("./src/routes/adminSignup"));
const getCartItems_1 = __importDefault(require("./src/routes/getCartItems"));
const getOrderStatistics_1 = __importDefault(require("./src/routes/getOrderStatistics"));
const generateDiscountCode_1 = __importDefault(require("./src/routes/generateDiscountCode"));
const app = (0, express_1.default)();
app.use(body_parser_1.default.json());
app.use((0, cors_1.default)());
app.use(signup_1.default);
app.use(signin_1.default);
app.use(checkout_1.default);
app.use(addToCart_1.default);
app.use(createItem_1.default);
app.use(adminSignup_1.default);
app.use(getCartItems_1.default);
app.use(getOrderStatistics_1.default);
app.use(generateDiscountCode_1.default);
const PORT = process.env.PORT || 5000;
mongoose_1.default.set("strictQuery", true);
mongoose_1.default
    .connect("mongodb://localhost:27017/ecommerce")
    .then(() => app.listen(PORT, () => console.log(`Server running on port: ${PORT}`)))
    .catch((error) => console.log(error.message));
exports.default = app;
