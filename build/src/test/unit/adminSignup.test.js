"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = __importStar(require("chai"));
const chai_http_1 = __importDefault(require("chai-http"));
const index_1 = __importDefault(require("../../../index"));
const User_1 = __importDefault(require("../../models/User"));
chai_1.default.use(chai_http_1.default);
describe("Admin signup", () => {
    before(() => __awaiter(void 0, void 0, void 0, function* () {
        yield User_1.default.deleteMany({});
    }));
    it("should sign up a new admin successfully", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield chai_1.default.request(index_1.default).post("/api/admin/signup").send({
            email: "admin@test.com",
            password: "password",
            name: "Test Admin",
        });
        (0, chai_1.expect)(res).to.have.status(201);
        (0, chai_1.expect)(res.body.jwt).to.be.a("string");
        (0, chai_1.expect)(res.body.user.email).to.equal("admin@test.com");
        (0, chai_1.expect)(res.body.user.isAdmin).to.be.true;
        (0, chai_1.expect)(res.body.message).to.equal("User created successfully");
    }));
    it("should not sign up admin with invalid email", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield chai_1.default.request(index_1.default).post("/api/admin/signup").send({
            email: "invalidemail",
            password: "password",
            name: "Test Admin",
        });
        (0, chai_1.expect)(res).to.have.status(400);
        // expect(res.body.message).to.equal("Email must be valid");
    }));
    it("should not sign up admin with invalid password", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield chai_1.default.request(index_1.default).post("/api/admin/signup").send({
            email: "admin@test.com",
            password: "p",
            name: "Test Admin",
        });
        (0, chai_1.expect)(res).to.have.status(400);
        // expect(res.body.message).to.equal(
        //   "Password must be between 4 and 20 characters"
        // );
    }));
    it("should not sign up admin with already existing email", () => __awaiter(void 0, void 0, void 0, function* () {
        yield chai_1.default.request(index_1.default).post("/api/admin/signup").send({
            email: "admin@test.com",
            password: "password",
            name: "Test Admin",
        });
        const res = yield chai_1.default.request(index_1.default).post("/api/admin/signup").send({
            email: "admin@test.com",
            password: "password",
            name: "Test Admin",
        });
        (0, chai_1.expect)(res).to.have.status(400);
        (0, chai_1.expect)(res.body.message).to.equal("Account already in use");
    }));
});
