"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const chai_http_1 = __importDefault(require("chai-http"));
const chai_1 = __importStar(require("chai"));
const index_1 = __importDefault(require("../../../index"));
const User_1 = __importDefault(require("../../models/User"));
const CartItem_1 = __importDefault(require("../../models/CartItem"));
chai_1.default.use(chai_http_1.default);
describe("Add to Cart Route", () => {
    let cartItem;
    let user;
    const payload = {
        email: "testuser@test.com",
        isAdmin: false,
        name: "Test User",
    };
    const token = jsonwebtoken_1.default.sign(payload, process.env.JWT_KEY);
    beforeEach(() => __awaiter(void 0, void 0, void 0, function* () {
        cartItem = new CartItem_1.default({
            item: "item1",
            price: 10,
        });
        yield cartItem.save();
        user = new User_1.default({
            email: "testuser@test.com",
            password: "password",
            name: "Test User",
            cart: [],
            orders: [],
        });
        yield user.save();
    }));
    afterEach(() => __awaiter(void 0, void 0, void 0, function* () {
        yield CartItem_1.default.deleteMany({});
        yield User_1.default.deleteMany({});
    }));
    it("should add an item to the user's cart", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield chai_1.default
            .request(index_1.default)
            .post("/api/cart/add")
            .set("cookies", `jwt=${token}`)
            .send({ item: cartItem.item, quantity: 3 });
        (0, chai_1.expect)(res.status).to.equal(201);
        (0, chai_1.expect)(res.body.message).to.equal("Item added to cart successfully");
        (0, chai_1.expect)(res.body.item.item).to.equal(cartItem.item.toString());
        const updatedUser = yield User_1.default.findOne({ email: user.email });
        (0, chai_1.expect)(updatedUser.cart).to.have.lengthOf(1);
        (0, chai_1.expect)(updatedUser.cart[0].item).to.equal(cartItem.item.toString());
        (0, chai_1.expect)(updatedUser.cart[0].quantity).to.equal(3);
    }));
    it("should return error if item is not found", () => __awaiter(void 0, void 0, void 0, function* () {
        const res = yield chai_1.default
            .request(index_1.default)
            .post("/api/cart/add")
            .set("cookies", `jwt=${token}`)
            .send({ item: "5f9f1c8a6aefa53d62f36f0c", quantity: 3 });
        (0, chai_1.expect)(res.status).to.equal(400);
        (0, chai_1.expect)(res.body.error).to.equal("Item not found");
    }));
});
