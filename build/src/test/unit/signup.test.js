"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = __importDefault(require("chai"));
const index_1 = __importDefault(require("../../../index"));
const chai_http_1 = __importDefault(require("chai-http"));
const User_1 = __importDefault(require("../../models/User"));
chai_1.default.use(chai_http_1.default);
chai_1.default.should();
beforeEach(() => __awaiter(void 0, void 0, void 0, function* () {
    yield User_1.default.deleteMany({});
}));
describe("Signup", () => {
    it("should create a new user", (done) => {
        const user = {
            name: "John Doe",
            email: "johndoe@gmail.com",
            password: "password",
        };
        chai_1.default
            .request(index_1.default)
            .post("/api/user/signup")
            .send({
            name: user.name,
            email: user.email,
            password: user.password,
        })
            .end((err, res) => {
            res.should.have.status(201);
            res.body.should.have
                .property("message")
                .eql("User created successfully");
            res.body.should.have.property("user");
            done();
        });
    });
    it("should not create a new user if email already exists", (done) => {
        const user = {
            name: "John Doe",
            email: "johndoe@gmail.com",
            password: "password",
        };
        chai_1.default
            .request(index_1.default)
            .post("/api/user/signup")
            .send({
            name: user.name,
            email: user.email,
            password: user.password,
        })
            .end((err, res) => {
            res.should.have.status(201);
            res.body.should.have
                .property("message")
                .eql("User created successfully");
        });
        chai_1.default
            .request(index_1.default)
            .post("/api/user/signup")
            .send({
            name: user.name,
            email: user.email,
            password: user.password,
        })
            .end((err, res) => {
            res.should.have.status(400);
            // res.body.should.have.property("message").eql("User already exists");
            done();
        });
    });
    it("should not create a new user if email is invalid", (done) => {
        const user = {
            name: "John Doe",
            email: "johndoe",
            password: "password",
        };
        chai_1.default
            .request(index_1.default)
            .post("/api/user/signup")
            .send({
            name: user.name,
            email: user.email,
            password: user.password,
        })
            .end((err, res) => {
            res.should.have.status(400);
            done();
        });
    });
    it("should not create a new user if password is invalid", (done) => {
        const user = {
            name: "John Doe",
            email: "johndoe@gmail.com",
            password: "pas",
        };
        chai_1.default
            .request(index_1.default)
            .post("/api/user/signup")
            .send({
            name: user.name,
            email: user.email,
            password: user.password,
        })
            .end((err, res) => {
            res.should.have.status(400);
            done();
        });
    });
});
