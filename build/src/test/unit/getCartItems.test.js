"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = __importStar(require("chai"));
const chai_http_1 = __importDefault(require("chai-http"));
const index_1 = __importDefault(require("../../../index"));
const User_1 = __importDefault(require("../../models/User"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
chai_1.default.use(chai_http_1.default);
describe("Get Cart", () => {
    let user;
    const payload = {
        email: "test@email.com",
        name: "testName",
        isAdmin: false,
    };
    const token = jsonwebtoken_1.default.sign(payload, process.env.JWT_KEY);
    beforeEach(() => __awaiter(void 0, void 0, void 0, function* () {
        user = new User_1.default({
            name: "testName",
            email: "test@email.com",
            password: "testpassword",
            cart: [
                {
                    item: "item1",
                    quantity: 1,
                    price: 20,
                },
                {
                    item: "item2",
                    quantity: 2,
                    price: 20,
                },
            ],
        });
        yield user.save();
    }));
    afterEach(() => __awaiter(void 0, void 0, void 0, function* () {
        yield User_1.default.deleteMany({});
    }));
    it("Should get the cart of a user", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield chai_1.default
            .request(index_1.default)
            .get("/api/cart")
            .set("cookies", `jwt=${token}`);
        (0, chai_1.expect)(response).to.have.status(200);
        (0, chai_1.expect)(response.body.cartItems).to.have.lengthOf(2);
        (0, chai_1.expect)(response.body.cartItems[0].item).to.equal("item1");
        (0, chai_1.expect)(response.body.cartItems[0].quantity).to.equal(1);
        (0, chai_1.expect)(response.body.cartItems[1].item).to.equal("item2");
        (0, chai_1.expect)(response.body.cartItems[1].quantity).to.equal(2);
    }));
    it("Should return an error if the user is not found", () => __awaiter(void 0, void 0, void 0, function* () {
        yield User_1.default.deleteMany({});
        const response = yield chai_1.default
            .request(index_1.default)
            .get("/api/cart")
            .set("cookies", `jwt=${token}`);
        (0, chai_1.expect)(response).to.have.status(400);
        (0, chai_1.expect)(response.body.error).to.equal("User not found");
    }));
    it("Should return an error if the user is not authenticated", () => __awaiter(void 0, void 0, void 0, function* () {
        const response = yield chai_1.default.request(index_1.default).get("/api/cart");
        (0, chai_1.expect)(response).to.have.status(401);
        (0, chai_1.expect)(response.body.error).to.equal("You must be signed in");
    }));
});
