"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cartItemSchema = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
// Define the schema for the cart item
exports.cartItemSchema = new mongoose_1.default.Schema({
    item: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
});
const CartItem = mongoose_1.default.model("CartItem", exports.cartItemSchema);
exports.default = CartItem;
