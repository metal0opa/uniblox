"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const CartItem_1 = require("./CartItem");
// Define the schema for the order
const orderSchema = new mongoose_1.default.Schema({
    items: [CartItem_1.cartItemSchema],
    totalCost: {
        type: Number,
        required: true,
    },
    discount: {
        type: mongoose_1.default.Schema.Types.ObjectId,
        ref: "DiscountCode",
    },
    date: {
        type: Date,
        default: Date.now,
    },
});
// Create the model for the order
const Order = mongoose_1.default.model("Order", orderSchema);
exports.default = Order;
