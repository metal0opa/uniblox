"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
// Define the schema for the discount code
const discountCodeSchema = new mongoose_1.default.Schema({
    code: {
        type: String,
        required: true,
    },
    discountPercent: {
        type: Number,
        required: true,
    },
});
// Create the model for the discount code
const DiscountCode = mongoose_1.default.model("DiscountCode", discountCodeSchema);
exports.default = DiscountCode;
