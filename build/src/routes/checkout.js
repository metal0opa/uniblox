"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Order_1 = __importDefault(require("../models/Order"));
const DiscountCode_1 = __importDefault(require("../models/DiscountCode"));
const currentUser_1 = require("../middlewares/currentUser");
const User_1 = __importDefault(require("../models/User"));
const router = express_1.default.Router();
// Define the route to checkout
router.post("/api/cart/checkout", currentUser_1.currentUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const user = yield User_1.default.findOne({ email: (_a = req.user) === null || _a === void 0 ? void 0 : _a.email });
        if (!user)
            throw new Error("User not found");
        // Get the cart items
        const cartItems = user.cart;
        // Get the discount code
        const discountCode = yield DiscountCode_1.default.findOne({
            code: req.body.discountCode,
        });
        // Calculate the total price
        let totalPrice = 0;
        cartItems.forEach((item) => {
            totalPrice += item.quantity * item.price;
        });
        // Apply the discount code
        if (discountCode) {
            totalPrice =
                totalPrice - (totalPrice * discountCode.discountPercent) / 100;
        }
        // Create the order
        const order = new Order_1.default({
            user: user.id,
            items: cartItems,
            totalPrice,
            discountCode: discountCode ? discountCode.code : null,
        });
        // Clear the cart
        user.cart = [];
        user.orders.push(order);
        // Save the order
        yield order.save();
        yield user.save();
        res.json({ message: "Order placed successfully" });
    }
    catch (err) {
        res.status(500).json({ error: err.message });
    }
}));
exports.default = router;
