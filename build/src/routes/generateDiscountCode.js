"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const admin_1 = require("../middlewares/admin");
const currentUser_1 = require("../middlewares/currentUser");
const DiscountCode_1 = __importDefault(require("../models/DiscountCode"));
const Order_1 = __importDefault(require("../models/Order"));
const router = express_1.default.Router();
// Define the route to generate a discount code
router.post("/api/admin/generate-discount-code", currentUser_1.currentUser, admin_1.admin, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { code, discountPercent, orderId } = req.body;
        const newDiscountCode = new DiscountCode_1.default({ code, discountPercent });
        //  get the order and add the discount code to it
        const order = yield Order_1.default.findOne({
            _id: new mongoose_1.default.Types.ObjectId(orderId),
        });
        if (!order) {
            res.status(400).json({ error: "Invalid order id" });
            return;
        }
        order.discount = newDiscountCode;
        yield newDiscountCode.save();
        res.json({ message: "Discount code generated successfully" });
    }
    catch (err) {
        res.status(500).json({ error: err.message });
    }
}));
exports.default = router;
