"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Order_1 = __importDefault(require("../models/Order"));
const currentUser_1 = require("../middlewares/currentUser");
const admin_1 = require("../middlewares/admin");
const router = express_1.default.Router();
// Define the route to retrieve the order statistics
router.get("/api/admin/order-statistics", currentUser_1.currentUser, admin_1.admin, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const order = yield Order_1.default.findById(req.params.id).populate({
            path: "discount",
            model: "DiscountCode",
        });
        if (!order) {
            res.status(400).json({ error: "Invalid order id" });
            return;
        }
        let itemCount = 0;
        let totalCost = 0;
        let discountAmount = 0;
        const discountCodes = [];
        itemCount += order.items.length;
        totalCost += order.totalCost;
        discountAmount +=
            (order.discount.discountPercent / 100) * order.totalCost;
        discountCodes.push(order.discount);
        res.json({
            itemCount,
            totalCost,
            discountCodes,
            discountAmount,
        });
    }
    catch (err) {
        res.status(500).json({ error: err.message });
    }
}));
exports.default = router;
