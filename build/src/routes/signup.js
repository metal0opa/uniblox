"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const express_1 = __importDefault(require("express"));
const User_1 = __importDefault(require("../models/User"));
const common_1 = require("@metal_oopa/common");
const express_validator_1 = require("express-validator");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const router = express_1.default.Router();
router.post("/api/user/signup", [
    (0, express_validator_1.body)("email").isEmail().withMessage("Email must be valid"),
    (0, express_validator_1.body)("password")
        .trim()
        .isLength({ min: 4, max: 20 })
        .withMessage("Password must be between 4 and 20 characters"),
], common_1.validateRequest, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { email, password, name } = req.body;
        const existingUser = yield User_1.default.findOne({ email: email });
        if (existingUser) {
            throw new Error("User already exists");
        }
        let user;
        if (email && email.length > 0) {
            user = new User_1.default({ email, password, name, isAdmin: false });
            yield user.save();
            // Generate JWT
            const userJwt = jsonwebtoken_1.default.sign({
                email: user.email,
                name: user.name,
                isAdmin: user.isAdmin,
            }, process.env.JWT_KEY);
            // Store it on session object
            req.session = {
                jwt: userJwt,
            };
            res.status(201).send({
                jwt: userJwt,
                user: user,
                message: "User created successfully",
            });
        }
    }
    catch (err) {
        res.status(400).send({ message: err });
    }
}));
exports.default = router;
