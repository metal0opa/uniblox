"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const currentUser_1 = require("../middlewares/currentUser");
const express_1 = __importDefault(require("express"));
const CartItem_1 = __importDefault(require("../models/CartItem"));
const User_1 = __importDefault(require("../models/User"));
const router = express_1.default.Router();
router.post("/api/cart/add", currentUser_1.currentUser, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const { item, quantity } = req.body;
        const cartItem = yield CartItem_1.default.findOne({ item: item });
        if (!cartItem) {
            throw new Error("Item not found");
        }
        // Add the new cart item to the user's cart
        const user = yield User_1.default.findOne({ email: (_a = req.user) === null || _a === void 0 ? void 0 : _a.email });
        if (!user)
            throw new Error("User not found");
        user.cart.push({ quantity, item, price: cartItem.price });
        yield user.save();
        res
            .status(201)
            .json({ message: "Item added to cart successfully", item: cartItem });
    }
    catch (err) {
        res.status(400).json({ error: err.message });
    }
}));
exports.default = router;
