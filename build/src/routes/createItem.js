"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin_1 = require("../middlewares/admin");
const express_1 = __importDefault(require("express"));
const CartItem_1 = __importDefault(require("../models/CartItem"));
const currentUser_1 = require("../middlewares/currentUser");
const router = express_1.default.Router();
router.post("/api/items/create", currentUser_1.currentUser, admin_1.admin, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { item, price } = req.body;
        const newItem = new CartItem_1.default({ item, price });
        yield newItem.save();
        res
            .status(201)
            .json({ message: "Item created successfully", item: newItem });
    }
    catch (err) {
        res.status(400).json({ error: err.message });
    }
}));
exports.default = router;
