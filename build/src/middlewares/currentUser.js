"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.currentUser = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const currentUser = (req, res, next) => {
    try {
        if (req.headers &&
            req.headers.cookies &&
            req.headers.cookies.includes("jwt")) {
            const token = req.headers.cookies
                .toString()
                .split("jwt=")[1]
                .split(";")[0];
            if (!token || token === "undefined") {
                req.user = undefined;
            }
            else {
                const decoded = jsonwebtoken_1.default.verify(token.toString(), process.env.JWT_KEY);
                if (!decoded) {
                    //If some error occurs
                    res.status(401).json({
                        error: "You must be signed in",
                    });
                }
                else {
                    req.user = decoded;
                }
            }
            next();
        }
        else {
            res.status(401).send({
                error: "You must be signed in",
            });
        }
    }
    catch (e) {
        res.json({
            currentUser: undefined,
            message: "Malformed jwt token",
        });
    }
};
exports.currentUser = currentUser;
