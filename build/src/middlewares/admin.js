"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.admin = void 0;
const admin = (req, res, next) => {
    try {
        if (req.user && req.user.isAdmin && req.user.isAdmin === true) {
            next();
        }
        else {
            res.status(403).json({
                error: "You do not have permission",
            });
        }
    }
    catch (e) {
        res.json({
            error: "Invalid admin",
        });
    }
};
exports.admin = admin;
