import { Request, Response, NextFunction } from "express";
import jwt, { JwtPayload } from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config();

interface UserPayLoad {
  email?: string;
  name?: string;
  isAdmin?: boolean;
}

declare global {
  namespace Express {
    interface Request {
      user?: UserPayLoad;
    }
  }
}
const currentUser = (req: Request, res: Response, next: NextFunction) => {
  try {
    if (
      req.headers &&
      req.headers.cookies &&
      req.headers.cookies.includes("jwt")
    ) {
      const token = req.headers.cookies
        .toString()
        .split("jwt=")[1]
        .split(";")[0];
      if (!token || token === "undefined") {
        req.user = undefined;
      } else {
        const decoded = jwt.verify(token.toString(), process.env.JWT_KEY!);
        if (!decoded) {
          //If some error occurs
          res.status(401).json({
            error: "You must be signed in",
          });
        } else {
          req.user = decoded as UserPayLoad;
        }
      }
      next();
    } else {
      res.status(401).send({
        error: "You must be signed in",
      });
    }
  } catch (e) {
    res.json({
      currentUser: undefined,
      message: "Malformed jwt token",
    });
  }
};

export { currentUser };
