import { Request, Response, NextFunction } from "express";

const admin = (req: Request, res: Response, next: NextFunction) => {
  try {
    if (req.user && req.user.isAdmin && req.user.isAdmin === true) {
      next();
    } else {
      res.status(403).json({
        error: "You do not have permission",
      });
    }
  } catch (e) {
    res.json({
      error: "Invalid admin",
    });
  }
};

export { admin };
