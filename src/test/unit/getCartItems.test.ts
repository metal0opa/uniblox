import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import app from "../../../index";
import User from "../../models/User";
import jwt from "jsonwebtoken";

chai.use(chaiHttp);

describe("Get Cart", () => {
  let user;
  const payload = {
    email: "test@email.com",
    name: "testName",
    isAdmin: false,
  };
  const token = jwt.sign(payload, process.env.JWT_KEY!);

  beforeEach(async () => {
    user = new User({
      name: "testName",
      email: "test@email.com",
      password: "testpassword",
      cart: [
        {
          item: "item1",
          quantity: 1,
          price: 20,
        },
        {
          item: "item2",
          quantity: 2,
          price: 20,
        },
      ],
    });
    await user.save();
  });

  afterEach(async () => {
    await User.deleteMany({});
  });

  it("Should get the cart of a user", async () => {
    const response = await chai
      .request(app)
      .get("/api/cart")
      .set("cookies", `jwt=${token}`);

    expect(response).to.have.status(200);
    expect(response.body.cartItems).to.have.lengthOf(2);
    expect(response.body.cartItems[0].item).to.equal("item1");
    expect(response.body.cartItems[0].quantity).to.equal(1);
    expect(response.body.cartItems[1].item).to.equal("item2");
    expect(response.body.cartItems[1].quantity).to.equal(2);
  });

  it("Should return an error if the user is not found", async () => {
    await User.deleteMany({});

    const response = await chai
      .request(app)
      .get("/api/cart")
      .set("cookies", `jwt=${token}`);

    expect(response).to.have.status(400);
    expect(response.body.error).to.equal("User not found");
  });

  it("Should return an error if the user is not authenticated", async () => {
    const response = await chai.request(app).get("/api/cart");

    expect(response).to.have.status(401);
    expect(response.body.error).to.equal("You must be signed in");
  });
});
