import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";

import app from "../../../index";
import User from "../../models/User";

chai.use(chaiHttp);
dotenv.config();

describe("Signin Route", () => {
  before(async () => {
    // Remove all users before each test
    await User.deleteMany({});
  });

  afterEach(async () => {
    // Remove all users after each test
    await User.deleteMany({});
  });

  it("Should signin the user and return a token", async () => {
    const user = new User({
      email: "johndoe@gmail.com",
      password: "password",
      name: "John Doe",
      isAdmin: false,
    });
    await user.save();

    const res = await chai
      .request(app)
      .post("/api/user/signin")
      .send({ email: "johndoe@gmail.com", password: "password" });

    expect(res.status).to.equal(200);
    expect(res.body).to.have.property("jwt");
    expect(res.body).to.have.property("user");
    expect(res.body).to.have.property("message");

    const decoded = jwt.verify(res.body.jwt, process.env.JWT_KEY!);
    expect(decoded).to.have.property("email", "johndoe@gmail.com");
    expect(decoded).to.have.property("name", "John Doe");
    expect(decoded).to.have.property("isAdmin", false);
  });

  it("Should return an error for invalid credentials", async () => {
    const user = new User({
      email: "johndoe@gmail.com",
      password: "password",
      name: "John Doe",
    });
    await user.save();

    const res = await chai
      .request(app)
      .post("/api/user/signin")
      .send({ email: "johndoe@gmail.com", password: "incorrect-password" });

    expect(res.status).to.equal(400);
    expect(res.body).to.have.property("message", "Invalid Credentials");
  });

  it("Should return an error for non-existing user", async () => {
    const res = await chai
      .request(app)
      .post("/api/user/signin")
      .send({ email: "johndoe@gmail.com", password: "password" });

    expect(res.status).to.equal(400);
    expect(res.body).to.have.property("message", "User does not exist!");
  });
});
