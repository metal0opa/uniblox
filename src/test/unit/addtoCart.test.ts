import jwt from "jsonwebtoken";
import chaiHttp from "chai-http";
import chai, { expect } from "chai";
import { Document, Types } from "mongoose";

import app from "../../../index";
import User from "../../models/User";
import CartItem, { ICartItem } from "../../models/CartItem";

chai.use(chaiHttp);

describe("Add to Cart Route", () => {
  let cartItem: Document<unknown, any, ICartItem> &
    ICartItem & { _id: Types.ObjectId };
  let user: Document<unknown, any, User> & User & { _id: Types.ObjectId };
  const payload = {
    email: "testuser@test.com",
    isAdmin: false,
    name: "Test User",
  };
  const token = jwt.sign(payload, process.env.JWT_KEY!);

  beforeEach(async () => {
    cartItem = new CartItem({
      item: "item1",
      price: 10,
    });

    await cartItem.save();

    user = new User({
      email: "testuser@test.com",
      password: "password",
      name: "Test User",
      cart: [],
      orders: [],
    });

    await user.save();
  });

  afterEach(async () => {
    await CartItem.deleteMany({});
    await User.deleteMany({});
  });

  it("should add an item to the user's cart", async () => {
    const res = await chai
      .request(app)
      .post("/api/cart/add")
      .set("cookies", `jwt=${token}`)
      .send({ item: cartItem.item, quantity: 3 });

    expect(res.status).to.equal(201);
    expect(res.body.message).to.equal("Item added to cart successfully");
    expect(res.body.item.item).to.equal(cartItem.item.toString());

    const updatedUser = await User.findOne({ email: user.email });
    expect(updatedUser!.cart).to.have.lengthOf(1);
    expect(updatedUser!.cart[0].item).to.equal(cartItem.item.toString());
    expect(updatedUser!.cart[0].quantity).to.equal(3);
  });

  it("should return error if item is not found", async () => {
    const res = await chai
      .request(app)
      .post("/api/cart/add")
      .set("cookies", `jwt=${token}`)
      .send({ item: "5f9f1c8a6aefa53d62f36f0c", quantity: 3 });

    expect(res.status).to.equal(400);
    expect(res.body.error).to.equal("Item not found");
  });
});
