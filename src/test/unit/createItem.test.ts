import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import app from "../../../index";
import jwt from "jsonwebtoken";

chai.use(chaiHttp);

describe("Create item route", () => {
  before(async () => {
    await chai.request(app).post("/api/admin/signup").send({
      email: "test@email.com",
      password: "testPassword",
      name: "testName",
    });
  });

  it("Should return 401 status code if the user is not signed in", async () => {
    const response = await chai.request(app).post("/api/items/create").send({
      name: "test item",
      price: 20,
      description: "test description",
      image: "test image",
      quantity: 5,
    });

    expect(response).to.have.status(401);
    expect(response.body.error).to.equal("You must be signed in");
  });

  it("Should return 403 status code if the user is not an admin", async () => {
    const payload = {
      email: "test@email.com",
      name: "testName",
      isAdmin: false,
    };
    const token = jwt.sign(payload, process.env.JWT_KEY!);

    const response = await chai
      .request(app)
      .post("/api/items/create")
      .set("cookies", `jwt=${token}`)
      .send({
        item: "test item",
        price: 20,
      });

    expect(response).to.have.status(403);
    expect(response.body.error).to.equal("You do not have permission");
  });

  it("Should return 201 status code and the newly created item if the request is valid", async () => {
    const payload = {
      email: "test@email.com",
      isAdmin: true,
      name: "testName",
    };
    const token = jwt.sign(payload, process.env.JWT_KEY!);

    const response = await chai
      .request(app)
      .post("/api/items/create")
      .set("cookies", `jwt=${token}`)
      .send({
        item: "test item",
        price: 20,
      });

    expect(response).to.have.status(201);
    expect(response.body.item).to.include({
      item: "test item",
      price: 20,
    });
  });
});
