import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import app from "../../../index";
import User from "../../models/User";

chai.use(chaiHttp);

describe("Admin signup", () => {
  before(async () => {
    await User.deleteMany({});
  });

  it("should sign up a new admin successfully", async () => {
    const res = await chai.request(app).post("/api/admin/signup").send({
      email: "admin@test.com",
      password: "password",
      name: "Test Admin",
    });

    expect(res).to.have.status(201);
    expect(res.body.jwt).to.be.a("string");
    expect(res.body.user.email).to.equal("admin@test.com");
    expect(res.body.user.isAdmin).to.be.true;
    expect(res.body.message).to.equal("User created successfully");
  });

  it("should not sign up admin with invalid email", async () => {
    const res = await chai.request(app).post("/api/admin/signup").send({
      email: "invalidemail",
      password: "password",
      name: "Test Admin",
    });

    expect(res).to.have.status(400);
    // expect(res.body.message).to.equal("Email must be valid");
  });

  it("should not sign up admin with invalid password", async () => {
    const res = await chai.request(app).post("/api/admin/signup").send({
      email: "admin@test.com",
      password: "p",
      name: "Test Admin",
    });

    expect(res).to.have.status(400);
    // expect(res.body.message).to.equal(
    //   "Password must be between 4 and 20 characters"
    // );
  });

  it("should not sign up admin with already existing email", async () => {
    await chai.request(app).post("/api/admin/signup").send({
      email: "admin@test.com",
      password: "password",
      name: "Test Admin",
    });

    const res = await chai.request(app).post("/api/admin/signup").send({
      email: "admin@test.com",
      password: "password",
      name: "Test Admin",
    });

    expect(res).to.have.status(400);
    expect(res.body.message).to.equal("Account already in use");
  });
});
