import chai from "chai";
import app from "../../../index";
import chaiHttp from "chai-http";
import User from "../../models/User";

chai.use(chaiHttp);
chai.should();

beforeEach(async () => {
  await User.deleteMany({});
});

describe("Signup", () => {
  it("should create a new user", (done) => {
    const user = {
      name: "John Doe",
      email: "johndoe@gmail.com",
      password: "password",
    };

    chai
      .request(app)
      .post("/api/user/signup")
      .send({
        name: user.name,
        email: user.email,
        password: user.password,
      })
      .end((err, res) => {
        res.should.have.status(201);
        res.body.should.have
          .property("message")
          .eql("User created successfully");
        res.body.should.have.property("user");
        done();
      });
  });

  it("should not create a new user if email already exists", (done) => {
    const user = {
      name: "John Doe",
      email: "johndoe@gmail.com",
      password: "password",
    };

    chai
      .request(app)
      .post("/api/user/signup")
      .send({
        name: user.name,
        email: user.email,
        password: user.password,
      })
      .end((err, res) => {
        res.should.have.status(201);
        res.body.should.have
          .property("message")
          .eql("User created successfully");
      });

    chai
      .request(app)
      .post("/api/user/signup")
      .send({
        name: user.name,
        email: user.email,
        password: user.password,
      })
      .end((err, res) => {
        res.should.have.status(400);
        // res.body.should.have.property("message").eql("User already exists");
        done();
      });
  });

  it("should not create a new user if email is invalid", (done) => {
    const user = {
      name: "John Doe",
      email: "johndoe",
      password: "password",
    };

    chai
      .request(app)
      .post("/api/user/signup")
      .send({
        name: user.name,
        email: user.email,
        password: user.password,
      })
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
  });

  it("should not create a new user if password is invalid", (done) => {
    const user = {
      name: "John Doe",
      email: "johndoe@gmail.com",
      password: "pas",
    };

    chai
      .request(app)
      .post("/api/user/signup")
      .send({
        name: user.name,
        email: user.email,
        password: user.password,
      })

      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
  });
});
