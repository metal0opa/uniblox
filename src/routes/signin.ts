import jwt from "jsonwebtoken";
import express, { Request, Response } from "express";

import User from "../models/User";
import { Password } from "../services/Password";

import dotenv from "dotenv";
dotenv.config();

const router = express.Router();

router.post("/api/user/signin", async (req: Request, res: Response) => {
  try {
    const { email, password, accountWallet } = req.body;

    let existingUser: any;
    if (email && email.length > 0) existingUser = await User.findOne({ email });
    else existingUser = await User.findOne({ accountWallet });

    if (!existingUser) {
      throw new Error("User does not exist!");
    }

    if (existingUser.password) {
      const passwordsMatch = await Password.compare(
        existingUser.password!,
        password
      );
      if (!passwordsMatch) {
        throw new Error("Invalid Credentials");
      }
    }

    // Generate JWT
    const userJwt = jwt.sign(
      {
        email: existingUser.email,
        name: existingUser.name,
        isAdmin: existingUser.isAdmin,
      },
      process.env.JWT_KEY!
    );

    // Store it on session object
    req.session = {
      jwt: userJwt,
    };

    res.status(200).send({
      jwt: userJwt,
      user: existingUser,
      message: "User logged in successfully",
    });
  } catch (err: any) {
    res.status(400).send({ message: err.message });
  }
});

export default router;
