import { admin } from "../middlewares/admin";
import express from "express";
import CartItem from "../models/CartItem";
import { currentUser } from "../middlewares/currentUser";

const router = express.Router();

router.post("/api/items/create", currentUser, admin, async (req, res) => {
  try {
    const { item, price } = req.body;
    const newItem = new CartItem({ item, price });
    await newItem.save();

    res
      .status(201)
      .json({ message: "Item created successfully", item: newItem });
  } catch (err: any) {
    res.status(400).json({ error: err.message });
  }
});

export default router;
