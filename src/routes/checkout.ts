import express from "express";
import Order from "../models/Order";
import DiscountCode from "../models/DiscountCode";
import { currentUser } from "../middlewares/currentUser";
import User from "../models/User";

const router = express.Router();

// Define the route to checkout
router.post("/api/cart/checkout", currentUser, async (req, res) => {
  try {
    const user = await User.findOne({ email: req.user?.email });

    if (!user) throw new Error("User not found");

    // Get the cart items
    const cartItems = user.cart;

    // Get the discount code
    const discountCode = await DiscountCode.findOne({
      code: req.body.discountCode,
    });

    // Calculate the total price
    let totalPrice = 0;
    cartItems.forEach((item) => {
      totalPrice += item.quantity * item.price;
    });

    // Apply the discount code
    if (discountCode) {
      totalPrice =
        totalPrice - (totalPrice * discountCode.discountPercent) / 100;
    }

    // Create the order
    const order = new Order({
      user: user.id,
      items: cartItems,
      totalPrice,
      discountCode: discountCode ? discountCode.code : null,
    });

    // Clear the cart
    user.cart = [];
    user.orders.push(order);

    // Save the order
    await order.save();
    await user.save();

    res.json({ message: "Order placed successfully" });
  } catch (err: any) {
    res.status(500).json({ error: err.message });
  }
});

export default router;
