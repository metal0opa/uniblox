import jwt from "jsonwebtoken";
import express, { Request, Response } from "express";
import { body } from "express-validator";
import User from "../models/User";
import { validateRequest } from "@metal_oopa/common";

import dotenv from "dotenv";
dotenv.config();

const router = express.Router();

router.post(
  "/api/admin/signup",
  [
    body("email").isEmail().withMessage("Email must be valid"),
    body("password")
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage("Password must be between 4 and 20 characters"),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    try {
      const { email, password, name } = req.body;
      const existingUser = await User.findOne({ email: email });

      if (existingUser) {
        throw new Error("Account already in use");
      }

      let user;
      if (email && email.length > 0) {
        user = new User({ email, password, name, isAdmin: true });
        await user.save();

        // Generate JWT
        const userJwt = jwt.sign(
          {
            email: user.email,
            name: user.name,
            isAdmin: user.isAdmin,
          },
          process.env.JWT_KEY!
        );

        // Store it on session object
        req.session = {
          jwt: userJwt,
        };

        res.status(201).send({
          jwt: userJwt,
          user: user,
          message: "User created successfully",
        });
      }
    } catch (err: any) {
      res.status(400).send({ message: err.message });
    }
  }
);

export default router;
