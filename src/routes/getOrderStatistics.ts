import express from "express";
import Order from "../models/Order";
import { IDiscountCode } from "../models/DiscountCode";
import { currentUser } from "../middlewares/currentUser";
import { admin } from "../middlewares/admin";

const router = express.Router();

// Define the route to retrieve the order statistics
router.get(
  "/api/admin/order-statistics",
  currentUser,
  admin,
  async (req, res) => {
    try {
      const order = await Order.findById(req.params.id).populate({
        path: "discount",
        model: "DiscountCode",
      });

      if (!order) {
        res.status(400).json({ error: "Invalid order id" });
        return;
      }
      let itemCount = 0;
      let totalCost = 0;
      let discountAmount = 0;
      const discountCodes: IDiscountCode[] = [];
      itemCount += order.items.length;
      totalCost += order.totalCost;
      discountAmount +=
        (order.discount.discountPercent / 100) * order.totalCost;
      discountCodes.push(order.discount);
      res.json({
        itemCount,
        totalCost,
        discountCodes,
        discountAmount,
      });
    } catch (err: any) {
      res.status(500).json({ error: err.message });
    }
  }
);

export default router;
