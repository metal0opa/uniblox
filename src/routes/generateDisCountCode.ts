import express from "express";
import mongoose from "mongoose";
import { admin } from "../middlewares/admin";
import { currentUser } from "../middlewares/currentUser";
import DiscountCode from "../models/DiscountCode";
import Order from "../models/Order";

const router = express.Router();

// Define the route to generate a discount code
router.post(
  "/api/admin/generate-discount-code",
  currentUser,
  admin,
  async (req, res) => {
    try {
      const { code, discountPercent, orderId } = req.body;
      const newDiscountCode = new DiscountCode({ code, discountPercent });

      //  get the order and add the discount code to it
      const order = await Order.findOne({
        _id: new mongoose.Types.ObjectId(orderId),
      });

      if (!order) {
        res.status(400).json({ error: "Invalid order id" });
        return;
      }

      order.discount = newDiscountCode;

      await newDiscountCode.save();
      res.json({ message: "Discount code generated successfully" });
    } catch (err: any) {
      res.status(500).json({ error: err.message });
    }
  }
);

export default router;
