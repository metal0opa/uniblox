import { currentUser } from "../middlewares/currentUser";
import express from "express";
import CartItem from "../models/CartItem";
import User from "../models/User";

const router = express.Router();

router.post("/api/cart/add", currentUser, async (req, res) => {
  try {
    const { item, quantity } = req.body;
    const cartItem = await CartItem.findOne({ item: item });

    if (!cartItem) {
      throw new Error("Item not found");
    }

    // Add the new cart item to the user's cart
    const user = await User.findOne({ email: req.user?.email });
    if (!user) throw new Error("User not found");
    user.cart.push({ quantity, item, price: cartItem.price });

    await user.save();
    res
      .status(201)
      .json({ message: "Item added to cart successfully", item: cartItem });
  } catch (err: any) {
    res.status(400).json({ error: err.message });
  }
});

export default router;
