import { currentUser } from "../middlewares/currentUser";
import express from "express";
import User from "../models/User";

const router = express.Router();

router.get("/api/cart", currentUser, async (req, res) => {
  try {
    const user = await User.findOne({ email: req.user?.email });

    if (!user) throw new Error("User not found");

    res.json({ cartItems: user.cart });
  } catch (err: any) {
    res.status(400).json({ error: err.message });
  }
});

export default router;
