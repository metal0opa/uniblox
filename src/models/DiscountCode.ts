import mongoose from "mongoose";

// Define the interface for the discount code
export interface IDiscountCode {
  code: string;
  discountPercent: number;
}

// Define the schema for the discount code
const discountCodeSchema = new mongoose.Schema({
  code: {
    type: String,
    required: true,
  },
  discountPercent: {
    type: Number,
    required: true,
  },
});

// Create the model for the discount code
const DiscountCode = mongoose.model<IDiscountCode>(
  "DiscountCode",
  discountCodeSchema
);

export default DiscountCode;
