import mongoose from "mongoose";
import { ICartItem, cartItemSchema } from "./CartItem";
import { IDiscountCode } from "./DiscountCode";

// Define the interface for the order
export interface IOrder {
  items: ICartItem[];
  totalCost: number;
  discount: IDiscountCode;
  date: Date;
}

// Define the schema for the order
const orderSchema = new mongoose.Schema({
  items: [cartItemSchema],
  totalCost: {
    type: Number,
    required: true,
  },
  discount: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "DiscountCode",
  },
  date: {
    type: Date,
    default: Date.now,
  },
});

// Create the model for the order
const Order = mongoose.model<IOrder>("Order", orderSchema);

export default Order;
