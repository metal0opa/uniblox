import mongoose from "mongoose";

// Define the interface for the cart item
export interface ICartItem {
  item: string;
  price: number;
}

// Define the schema for the cart item
export const cartItemSchema = new mongoose.Schema({
  item: {
    type: String,
    required: true,
  },

  price: {
    type: Number,
    required: true,
  },
});

const CartItem = mongoose.model<ICartItem>("CartItem", cartItemSchema);

export default CartItem;
