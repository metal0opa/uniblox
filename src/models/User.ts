import mongoose from "mongoose";
import { Password } from "../services/Password";
import { IOrder } from "./Order";

interface ICartItem {
  item: string;
  quantity: number;
  price: number;
}

// Define the interface for the order
interface User {
  name: string;
  email: string;
  password: string;
  isAdmin: boolean;
  cart: Array<ICartItem>;
  orders: Array<IOrder>;
}

// Define the schema for the order
const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  cart: {
    type: Array,
    default: [],
    required: false,
  },
  orders: {
    type: Array,
    default: [],
    required: false,
  },
});

userSchema.pre("save", async function (done) {
  if (this.isModified("password")) {
    const hashed = await Password.toHash(this.get("password"));
    this.set("password", hashed);
  }
  done();
});

userSchema.statics.build = (attrs: User) => {
  return new User(attrs);
};

// Create the model for the order
const User = mongoose.model<User>("User", userSchema);

export default User;
