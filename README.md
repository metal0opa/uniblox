# uniblox Ecommerce Store

## Description

Clients can add items to their cart and checkout to successfully place an order. Every nth order gets a coupon code for 10% discount and can apply to their cart.

We would like you to design and implement APIs for adding items to cart and checkout functionality. The checkout API would validate if the discount code is valid before giving the discount.

Building a UI that showcases the functionality is a stretch goal. If you are primarily a backend engineer, you can also submit postman or REST client or equivalent.

The store also has two admin API's:

Generate a discount code if the condition above is satisfied.
Lists count of items purchased, total purchase amount, list of discount codes and total discount amount.

## How to run

### Prerequisites

1. Install [Node.js](https://nodejs.org/en/download/)
2. Install [MongoDB](https://docs.mongodb.com/manual/installation/)

### Steps

1.  Clone the repository
2.  `cd` into the project directory
3.  Run local mongoDB server on port 27017
4.  Run `npm install` to install dependencies
5.  Run `npm start` to start the server
6.  Run `npm test` to run tests

## API Documentation

1. Add item to cart - `POST /api/cart/add`
2. Checkout - `POST /api/cart/checkout`
3. Generate discount code - `POST /api/admin/generate-discount-code`
4. Signup - `POST /api/user/signup`
5. Signin - `POST /api/user/signin`
6. Admin signup - `POST /api/admin/signup`
7. create item - `POST /api/item/create`
8. get order stats - `GET /api/admin/order-statsistics`
