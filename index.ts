import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import mongoose from "mongoose";

import signup from "./src/routes/signup";
import signin from "./src/routes/signin";
import checkout from "./src/routes/checkout";
import addToCart from "./src/routes/addToCart";
import createItem from "./src/routes/createItem";
import adminSignup from "./src/routes/adminSignup";
import getCartItems from "./src/routes/getCartItems";
import getOrderStatistics from "./src/routes/getOrderStatistics";
import generateDiscountCode from "./src/routes/generateDiscountCode";

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.use(signup);
app.use(signin);
app.use(checkout);
app.use(addToCart);
app.use(createItem);
app.use(adminSignup);
app.use(getCartItems);
app.use(getOrderStatistics);
app.use(generateDiscountCode);

const PORT = process.env.PORT || 5000;

mongoose.set("strictQuery", true);
mongoose
  .connect("mongodb://localhost:27017/ecommerce")

  .then(() =>
    app.listen(PORT, () => console.log(`Server running on port: ${PORT}`))
  )

  .catch((error) => console.log(error.message));

export default app;
